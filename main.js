const openBtn = document.querySelector('#open-btn');
const closeBtn = document.querySelector('#close-btn');
const active = document.querySelector('#active');
const btn = document.querySelector('#btn');
const navbar = document.querySelector('#navbar');

btn.addEventListener('click', () => {
  console.log('btn-clicked');
  active.classList.toggle('active');

  if (closeBtn.classList.contains('hidden')) {
    openBtn.classList.add('hidden');
    closeBtn.classList.remove('hidden');
  } else {
    openBtn.classList.remove('hidden');
    closeBtn.classList.add('hidden');
  }
});

window.addEventListener('scroll', () => {
  if (window.scrollY > 50) {
    navbar.classList.add('show');
  } else {
    navbar.classList.remove('show');
  }
});
